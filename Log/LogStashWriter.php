<?php

class LogStashWriter
{

    use ZeroMQPushPull;

    const ZMQ_DAEMON_OFF = true;
    const ZMQ_DAEMON_BIND_ON_PULL = true;

    static function write($message)
    {
        static $hostname;
        $time = isset($message['@timestamp'])?$message['@timestamp']:microtime(true);
        $info_keys = [
            '@timestamp' => date('Y-m-d\TH:i:s.'.substr($time, 11, 3).'O', $time),
            'host' => $hostname?:$hostname = gethostname(),
        ];
        if(!is_array($message)) {
            $message = ['message' => $message];
        }
        if(!isset($message['project'])) {
            $info_keys['project'] = PROJECT;
        }
        if(!isset($message['rev'])) {
            $info_keys['rev'] = PROJECTREV;
        }
        self::ZMQPush(
            JSON::encode(
                $info_keys +
                $message
            )
        );
    }

    static function ZMQGetDsn()
    {
        return "tcp://" . Project::getConfig()['logger'];
    }

    static function fromStdin($grok_pattern = null, $add_keys = [], $popen_cmd = null) {
        $Grok = new Grok;

        if($popen_cmd) {
            $stream = popen("exec ".$popen_cmd, "r");
        } else {
            $stream = STDIN;
        }

        while(!feof($stream)) {
            $log_string = fread($stream, 10000000);
            if(trim($log_string) == "") {
                continue;
            }
            $msg = ['message' => $log_string];
            if($grok_pattern) {
                $msg = $Grok->parse($grok_pattern, $log_string) + $msg;
            }
            self::write($add_keys + $msg);
        }
    }

}