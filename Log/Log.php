<?php

class Log {

    use ZeroMQPushPull;

    const ZMQ_DAEMON_BIND_ON_PULL = 1;
    const ZMQ_DAEMON_MAX_ONE_TIME_MESSAGES = 1000;
    const ZMQ_DAEMON_RESTART_INTERVAL = 60;
    const ZMQ_DAEMON_USLEEP_AFTER_EMPTY_READ = 200000;

    static function info($msg, $source = 'common') {
        self::write($msg, $source, __FUNCTION__);
    }

    static function warn($msg, $source = 'common') {
        self::write($msg, $source, __FUNCTION__);
    }

    static function error($msg, $source = 'common') {
        self::write($msg, $source, __FUNCTION__);
    }

    static function security($msg, $source = 'common') {
        self::write($msg, $source, __FUNCTION__);
    }

    static function write($msg, $source = 'common', $type = "info") {
        self::ZMQPush(JSON::encode(array_merge([
            "@timestamp" => microtime(true),
            "source" => $source,
            "type" => $type,
        ], is_array($msg) ? $msg : ["message" => $msg] )));
    }

    static function event(\_OS\Event $Event) {
        self::ZMQPush(JSON::encode([
            "@timestamp" => microtime(true),
            "type" => 'event',
            "source" => get_class(Context()),
            "event" => get_class($Event),
        ] + (array)$Event ));
    }

    static function ZMQPullMessageHandler($messages) {
        foreach($messages as $message) {
            LogStashWriter::write(
                JSON::decode($message)
            );
        }
    }
} 