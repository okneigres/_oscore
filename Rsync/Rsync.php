<?php

class Rsync {

    static function rsyncFrom($host, $dir, $share = 'data') {
        $port = Taskman::getPortFor(__CLASS__);
        passthru("rsync -rR --port=$port $host::$share/$dir ".PATH_DATA);
    }

    static function rsyncTo($host, $dir, $share = 'data') {
        $port = Taskman::getPortFor(__CLASS__);
        chdir(PATH_DATA);
        passthru("rsync -rR --port=$port $dir $host::$share");
        chdir(PATH_WORKDIR);
    }

    static function daemon() {
        CatchEvent(\_OS\Core\System_InitConfigs::class);
        $PROJECTENV = PROJECTENV;
        `touch $PROJECTENV/etc/rsync.conf`;

        Taskman::prepareConfigs(__DIR__."/tmp", PATH_ENV."/etc/rsync/");

        Taskman::installDaemonUnderTaskman(
            "rsync",
            "rsync --daemon --address=0.0.0.0 --port=23232 --no-detach --config=$PROJECTENV/etc/rsync/rsync.conf"
        );
    }
}
