<?php

abstract class BaseEntity {

    static $table;

    public function getRepository() {
        $class_repo = str_replace('Entity', '',  get_called_class()).'Repository';
        print $class_repo;
        if (class_exists($class_repo)) {
            return new $class_repo;
        }
        return new BaseRepository();
    }

    public function __construct($data = []) {
        foreach ($data as $field => $value) {
            $fields = explode('_', $field);
            foreach ($fields as &$field) {
                $field = ucfirst($field);
            }
            $field = implode('', $fields);
            $func = 'set'.$field;
            if (method_exists($this, $func)) {
                $this->$func($value);
            }
        }
    }

}
