<?php

trait DataModel {

    public $id;
    public $version;

    function get($field) {
        if(!isset(static::$fields[$field])) {
            throw new Exception('Field '.$field.' does not exists in class '.static::class);
        }
        return isset($this->data[$field])?$this->data[$field]:static::$fields[$field]['default'];
    }

    /**
    * @param mixed $value
    * @return $this
    */
    public function setId($value) {
        $this->id = $value;
        return $this;
    }

    /**
    * @param string $field
    * @param mixed $value
    * @return $this
    */
    function set($field, $value) {
        if(!isset(static::$fields[$field])) {
            throw new Exception('Field '.$field.' does not exists in class '.static::class);
        }
        $this->data[$field] = $value;
        return $this;
    }

    function &access($field) {
        if(!isset(static::$fields[$field])) {
            throw new Exception('Field '.$field.' does not exists in class '.static::class);
        }
        if(!isset($this->data[$field])) {
            $this->data[$field] = static::$fields[$field]['default'];
        };
        return $this->data[$field];
    }

    function getData() {
        $data = $this->data;
        foreach(static::$fields as $field => $params) {
            if(isset(static::$fields[$field]['default']) && !isset($data[$field])) {
                $data[$field] = static::$fields[$field]['default'];
            }
            if(isset($data[$field])) {
                switch($params['type']) {
                    case "int": $data[$field] = (int)$data[$field]; break;
                }
            }
        }
        return $data;
    }

    static function generateDataClass() {
        CatchEvent(System_InitConfigs);

        // check signature
        $rp = new ReflectionProperty(static::class, 'data');
        if(strpos($rp->getDocComment(), "@var ".static::class."Data") === false) {
            throw new Exception("Cannot find PHPDoc signature /** @var ".static::class."Data */ for field \$data");
        }

        $fields = [];
        foreach(static::$fields as $field => $params) {
            $fields[] = "    /** @var ".str_pad($params['type'], 8, " ")." */  public $$field;";
        }

        `mkdir -p tmp/DataModel`;
        file_put_contents("tmp/DataModel/".static::class."Data.php", "<?php\n\nclass ".static::class."Data {\n\n".implode("\n", $fields)."\n}");
    }

    public function jsonSerialize() {
        return ['id' => $this->id] + $this->getData();
    }

//
//    public function offsetExists($offset) {
//        return isset(self::$fields[$offset]);
//    }
//
//    public function offsetGet($offset) {
//        return $this->get($offset);
//    }
//
//    public function offsetSet($offset, $value) {
//        $this->set($offset, $value);
//    }
//
//    public function offsetUnset($offset){
//        unset($this->data[$offset]);
//    }
//
}
