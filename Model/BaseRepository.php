<?php

class BaseRepository {

    public function fabricateSql($sql, $params = []) {
        $statement = DB::connect()->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        $statement->execute($params);
        return $statement->fetchAll();
    }

    static function entity() {
        return str_replace('Repository', '', get_called_class()).'Entity';
    }

    static function table() {
        $entity = self::entity();
        return $entity::$table;
    }

    public function getEntityByIds($ids = []) {
        $sql = 'SELECT * FROM '.self::table().' WHERE id in ('.implode(', ', $ids).')';
        $entity = self::entity();
        $statement = DB::connect()->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        $statement->execute();
        $result = $statement->fetchAll();
        if (count($result)) {
            $return = [];
            foreach ($result as $res) {
                $return[] = new $entity($res);
            }
            return $return;
        }
    }

    public function getEntityById($id) {
        $sql = 'SELECT * FROM '.self::table().' WHERE id = :id';
        $entity = self::entity();
        $params = [
            ':id'   =>  $id,
        ];
        $result = $this->fabricateSql($sql, $params);
        if (isset($result[0])) {
            return new $entity($result[0]);
        }
        return false;
    }

    public function getEntityByFields($params = []) {
        $entity = self::entity();
        $where = $sth_params = [];
        $sql = 'SELECT * FROM '.self::table().' WHERE ';
        foreach ($params as $field => $value) {
            $where[] = $field . '=:' . $field;
            $sth_params[':'.$field] = $value;
        }
        $sql .= implode(' AND ', $where);
        $result = $this->fabricateSql($sql, $sth_params);
        if (count($result)) {
            if (count($result) > 1) {
                $return = [];
                foreach ($result as $res) {
                    $return[] = new $entity($res);
                }
                return $return;
            }
            return new $entity($result[0]);
        }
        return false;
    }

}