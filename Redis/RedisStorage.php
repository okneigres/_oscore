<?php

trait RedisStorage
{

    static function runRedisStorageDaemon()
    {
        CatchEvent([__CLASS__.__TRAIT__."Host" => [System_InitConfigs]]);
        $daemon_name = __CLASS__.__TRAIT__."Host";
        $port = Taskman::getPortFor(__CLASS__.__TRAIT__."Host");

        `mkdir -p \$PROJECTDATA/$daemon_name`;

        Taskman::prepareConfigs(__DIR__."/etc/", PROJECTENV."/etc/redis/$daemon_name/", [
            "%PORT%" => $port,
            "%ROLE%" => $daemon_name,
        ]);

        $cmd = 'redis-server '.PROJECTENV."/etc/redis/$daemon_name/redis.conf --port $port";
        Taskman::installDaemonUnderTaskman($daemon_name, $cmd);
    }

    static function RedisStorage()
    {
        static $RedisConnection;
        return
            $RedisConnection
            ?:$RedisConnection = new RedisClient([
            'host' => 'localhost',
            'port' => Taskman::getPortFor(__CLASS__.__TRAIT__."Host"),
            'db' => 0,
        ]);
    }

//    static function moveRedis($host_from, $host_to) {
//        BaseHost::run(func_get_args(), function($host_from, $host_to) {
//
//        });
//    }

    static function _RedisStorageMakeRole() {
        CatchEvent(System_InitConfigs);

        $RoleClass = __CLASS__.__TRAIT__."Host";

        $template = <<<EOF
<?php

class $RoleClass extends HostRole {

}

EOF;

        file_put_contents(PATH_TMP."/".$RoleClass.".php", $template);
    }

}
