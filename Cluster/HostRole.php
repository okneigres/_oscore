<?php

class HostRole extends Context {

    static function getAllDeclaredRoles() {
        CatchEvent(\_OS\Core\System_InitConfigs::class);

        $roles = [];

        \_OS\Autoloader::loadAll();
        foreach(get_declared_classes() as $class) {
            if(is_subclass_of($class, HostRole::class)) {
                $roles[] = $class;
            }
        }

        return $roles;
    }

    static function getRoles2Hosts() {
        static $roles;
        $config = Project::getConfig();
        if(!$roles) {
            if(isset($config['multirole']) && $config['multirole']) {
                foreach(self::getAllDeclaredRoles() as $role) {
                    $roles[$role] = ['localhost'];
                }
            } else {
                if(file_exists(PROJECTDATA."/roles2hosts.json")) {
                    $roles = JSON::decode(file_get_contents(PROJECTDATA."/roles2hosts.json"));
                } else {
                    throw new Exception("no file roles2hosts.json");
                }
            }
        }
        return $roles;
    }

    static function getRoleHosts($role, $return_once = false, $trim_username = false) {
        $roles = self::getRoles2Hosts();
        $returning = isset($roles[$role])?$roles[$role]:[];
        if($trim_username) {
            foreach($returning as &$host) {
                if($pos = strpos($host, "@")) {
                    $host = substr($host, $pos + 1);
                }
            }
        }
        return $return_once?$returning[0]:$returning;
    }

    static function writeRoles2Hosts() {
        CatchEvent([BaseHost::class => \_OS\Core\System_InitConfigs::class]);
        if(file_exists(PROJECTDATA."/roles2hosts.json")) {
            $roles2hosts = JSON::decode(file_get_contents(PROJECTDATA."/roles2hosts.json"));
        } else {
            $roles2hosts = [];
        }

        foreach(self::getAllDeclaredRoles() as $role) {
            if(!isset($roles2hosts[$role])) {
                $roles2hosts[$role] = [];
            }
        }

        file_put_contents(PROJECTDATA."/roles2hosts.json", JSON::hencode($roles2hosts));
    }


    static function getRolesByHost($host = null) {
        if(is_null($host)) {
            if(file_exists(PATH_DATA."/my.host")) {
                $host = file_get_contents(PATH_DATA."/my.host");
            } else {
                return [];
            }
        }
        $roles = [];
        $roles2hosts = self::getRoles2Hosts();
        foreach($roles2hosts as $role => $hosts) {
            if(in_array($host, $hosts)) {
                $roles[] = $role;
            }
        }
        return $roles;
    }

}