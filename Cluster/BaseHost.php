<?php

class BaseHost extends HostRole {

    use ZeroMQReqRep;

    function runCmd($arguments, $cmd) {
        if($Context = BaseHost::getInstance()) {
            return call_user_func_array($arguments, $cmd);
        } else {
            return self::ZeroMQReqRep_run($arguments, $cmd);
        }
    }

} 