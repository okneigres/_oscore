var Core = require(process.env.PROJECTPATH + '/core/_OS/Core/Core.js');

var net = require('net');
var child_process = require('child_process');
var fs = require('fs');

Taskman = {
    configfile: process.env.PROJECTENV + '/etc/taskman.conf',
    socketfile: process.env.PROJECTENV + '/var/taskman.sock',
    pidfile:    process.env.PROJECTENV + '/var/taskman.pid',
    revisionfile:    process.env.HOME + '/' + process.env.PROJECT + '/project-revision'
};

process.title = process.env.PROJECTENV + '/etc/taskman.conf';

var _wtchto;

/*
if(process.argv.length > 2) {

    // try to connect to socket and send command
    console.log(process.argv);
    return;
}
*/

Taskman.Server = {
    Init: new Core.EventPoint(),
    Started: new Core.EventPoint(),
    NewConnection: new Core.EventPoint(),
    ConnectionLost: new Core.EventPoint(),
    ConnectionKilled: new Core.EventPoint(),
    ConfigChanged: new Core.EventPoint(),
    NewCommandList: new Core.EventPoint(),
    lines: {},
    server: null,
    tryStartServer: function() {
        Core.CatchEvent(Taskman.Server.Init);

        var _this = this;

        fs.readFile(Taskman.pidfile, function(err, data) {
            if(err) {
                console.log('start on fileerr');
                start_server();
            } else {
                var pid = parseInt(data.toString());
                if(!pid) {
                    start_server();
                } else {
                    try {
                        process.kill(pid, 0);
                    } catch(e) {
                        console.log(e);
                        start_server();
                    }
                }
            }
        });

        function start_server() {
            fs.unlink(Taskman.socketfile, function() {

            });
            fs.writeFile(Taskman.pidfile, process.pid.toString(), function(err, cb) {
                if(err) {
                    console.log('Cannot write');
                }
            });
            _this.server = net.createServer(function(conn){
                new Taskman.Server.NewConnection(conn);
            });
            _this.server.listen(Taskman.socketfile);
            new Taskman.Server.Started( { server: _this.server } );
        }
    },
//    registerConnection: function() {
//        var e = Core.CatchEvent(Taskman.Server.NewConnection);
//
//    },
//    unregisterConnection: function() {
//        var e = Core.CatchEvent(Taskman.Server.ConnectionLost, Taskman.Server.ConnectionKilled);
//
//    },
    readAndParseFileAndMakeCommandList: function() {
        Core.CatchEvent(Taskman.Server.Started, Taskman.Server.ConfigChanged);
        var _this = this;
        fs.readFile(Taskman.configfile, function(err, data){
            if(err) {
                throw err;
            }
            var raw_lines = data.toString().split(/\n/);
            var lines = {};
            for(var i = 0; i < raw_lines.length; i++) {
                var line = raw_lines[i].replace(/[ \t]+/g, ' ').replace(/^ /, '').replace(/ $/, '');
                if(line && !line.match(/^#/)) {
                    lines[line] = true;
                }
            }
            var commands = [];
            for(var i in _this.lines) {
                if(!_this.lines.hasOwnProperty(i)) continue;
                if(!lines.hasOwnProperty(i)) {
                    commands.push(['stop', i]);
                }
            }
            for(var i in lines) {
                if(!lines.hasOwnProperty(i)) continue;
                if(!_this.lines.hasOwnProperty(i)) {
                    commands.push(['exec', i]);
                }
            }
            new Taskman.Server.NewCommandList({commands: commands});
        });
    },
    _watchTimeout: null,
    watchFile: function() {
        Core.CatchEvent(Taskman.Server.Started);
        var _this = this, watcher;
        fs.watchFile(Taskman.configfile, watcher = function () {
            // подождём 2 секунды, если будут ещё изменения - накопим их
            if(_this._watchTimeout) {
                clearTimeout(_this._watchTimeout);
            }
            _this._watchTimeout = setTimeout(function() {
                new Taskman.Server.ConfigChanged();
            }, 2000);
        });
        this.unwatchFile = function() {
            fs.unwatchFile(Taskman.configfile, watcher);
        }
    },
    unwatchFile: function() {
        // fill after watch (see upper)
    },
    watchRevFile: function() {
        Core.CatchEvent(Taskman.Server.Started);
        fs.watchFile(Taskman.revisionfile, function () {
            // подождём 100ms
            if(_wtchto) {
                clearTimeout(_wtchto);
            }
            _wtchto = setTimeout(function() {
                fs.readFile(Taskman.revisionfile, function(err, data) {
                    console.log('change revision to ' + data.toString());
                    process.env.PROJECTREV = data.toString();
                    console.log('process.env.PROJECTREV = ' + process.env.PROJECTREV);
                    process.env.PROJECTPATH = process.env.HOME + '/' + process.env.PROJECT + '/' + data.toString();
                    console.log('process.env.PROJECTPATH = ' + process.env.PROJECTPATH);
                    console.log('chdir to ' + process.env.PROJECTPATH + '/');
                    process.chdir(process.env.PROJECTPATH + '/');
                });
            }, 100);
        });
    },
    executeCommandsAfterUpdate: function() {
        var e = Core.CatchEvent(Taskman.Server.NewCommandList);
        var commands = e.commands;

        function next_command() {
            if(!commands.length) return;
            var command = commands.shift();
            switch (command[0]) {
                case 'exec':
                    Taskman.Server.startTaskForLine(command[1], next_command);
                    break;
                case 'stop':
                    Taskman.Server.stopProcessesForLine(command[1], next_command);
                    break;
            }
        }
        next_command();
    },
    startTaskForLine: function(line, cb) {
        console.log('start', line);
        var matches = line.match(/^(\*) ([^ ]+) (.+)$/);
        console.log(line, matches);
        var pattern = matches[1];
        var name = matches[2];
        var cmdline = matches[3];
        var task = new RestartableTask(name, cmdline);
        this.lines[line] = {
            task: task
        };
        cb();
    },
    stopProcessesForLine: function(line, cb) {
        console.log('stop', line);
        var _this = this;
        this.lines[line].task.stopRespawning();
        this.kill(this.lines[line].task, function() {
            if(cb instanceof Function) cb();
        });
        delete _this.lines[line];
    },
    startup_stored_daemons: function() {
        fs.exists(taskman_daemon.daemons_file, function(result) {
            if(!result)
                return;
            fs.readFile(taskman_daemon.daemons_file, function(err, data) {
                console.log("read file");
                if(err)
                    throw err;
                try {
                    var daemons = JSON.parse(data);
                    for(var i = 0; i < daemons.length; i++) {
                        taskman_daemon.run_daemon(daemons[i].tags,daemons[i].cmdline);
                    }
                } catch(err) {
                    console.log(err);
                }
            });
        });
    },
    sync_write_daemons: function() {
        var daemons = [];
        for(var i = 0; i < taskman_daemon.watchlist.length; i++) {
            if(taskman_daemon.watchlist[i].is_daemon) {
                daemons.push({
                    tags: taskman_daemon.watchlist[i].tags,
                    cmdline: taskman_daemon.watchlist[i].cmdline
                });
            }
        }
        console.log("write file");
        fs.writeFile(taskman_daemon.daemons_file, JSON.stringify(daemons));
    },
    register_conn: function(conn) {
        conn.command = '';
        conn.on('data', function(data) {
            conn.command += data;
            try {
                if(conn.command.match(/^quit/))
                    return conn.destroy();
                var command = JSON.parse(conn.command);
                conn.command = '';
                console.log("execute_command", command);
                taskman_daemon.execute_connection_command(command, conn);
            } catch(e) {
                // read more packets to get real json command
                console.log("execute command failed, data: " + data.toString());
                console.error(e);
            }
        });

    },
    execute_connection_command: function(command, conn) {
        var cmd = command[0];
        var data = command.slice(1);
        switch(cmd) {
            case 'run_process':
                taskman_daemon.run_process(data[0],data[1]);
                break;
            case 'run_daemon':
                taskman_daemon.run_daemon(data[0],data[1]);
                break;
            case 'stop_process':
                taskman_daemon.stop_process(data[0], function() {
                    conn.destroy();
                });
                break;
            case 'stop_daemon':
                taskman_daemon.stop_daemon(data[0], function() {
                    conn.destroy();
                });
                break;
        }
    },
    register_process: function(process) {
        process.attached_conn = [];
        taskman_daemon.watchlist.push(process);
        process.stdout.on('data', function(data) {
            for(var i = 0;i < process.attached_conn.length; i++) {
                process.attached_conn[i].write(JSON.stringify({'stdout':data}));
            }
        });
        process.stderr.on('data', function() {
            for(var i = 0;i < process.attached_conn.length; i++) {
                process.attached_conn[i].write(JSON.stringify({'stderr':data}));
            }
        });
        process.on('exit', function(code) {
            for(var i = 0;i < process.attached_conn.length; i++) {
                process.attached_conn[i].write(JSON.stringify({'exit':code}));
            }
            for(var i = 0; i < taskman_daemon.watchlist.length; i++) {
                if(taskman_daemon.watchlist[i] == process)
                    taskman_daemon.watchlist.splice(i, 1);
            }
        });
    },
    run_process: function(tags, cmdline) {

        console.log(taskman_daemon.search_watchlist(tags, true).length);
        if(taskman_daemon.search_watchlist(tags, true).length)
            return;

        var process = child_process.spawn('bash', ['-c', cmdline]);
        process.tags = tags;
        process.cmdline = cmdline;
        process.is_daemon = false;
        taskman_daemon.register_process(process);
    },
    search_watchlist: function(tags, accurate) {
        var found = [];
        for(var i = 0; i < taskman_daemon.watchlist.length; i++) {
            var actual_tags = taskman_daemon.watchlist[i].tags;
            var found_tags = 0;
            if(accurate && actual_tags.length != tags.length)
                continue;
            for(var j = 0; j < actual_tags.length; j++) {
                for(var k=0; k < tags.length; k++) {
                    if(actual_tags[j] == tags[k])
                        found_tags++;
                }
            }
            if(found_tags == tags.length)
                found.push(taskman_daemon.watchlist[i]);
        }
        return found;
    },
    attach: function(tags) {
        not_implemented();
    },
    list: function(tags) {
        not_implemented();
    },
    kill: function(task, callback){

        if(!task.pid) {
            var _this = this;
            setTimeout(function(){
                _this.kill(task, callback);
            }, 250);
            return;
        }

        child_process.exec("ps x --no-heading -o pid,ppid", function(code, data, errdata) {
            if(errdata) {
                console.error(errdata);
            }
            var parsed = data.match(/(\d+) +(\d+)/g);
            for(var i = 0; i < parsed.length; i++) {
                parsed[i] = parsed[i].split(/ +/);
            }
            var pids_to_kill = [], pids_to_check = [task.pid];
            while(pids_to_check.length) {
                var pid = pids_to_check.shift();
                for(var i = 0; i < parsed.length; i++) {
                    if(parsed[i][1] == pid) {
                        pids_to_check.push(parsed[i][0]);
                    }
                }
                pids_to_kill.unshift(pid);
            }
            console.log('kill ' + pids_to_kill.join(' '));
            var kill_process = child_process.exec('kill ' + pids_to_kill.join(' '));
            kill_process.on("exit", function(code){ callback(); });
        });
    }
};

function RestartableTask(name, cmdline) {
    var _this = this;
    var restart = true;

    this.stopRespawning = function() {
        restart = false;
    };

    child_process.exec('for i in ' + cmdline + '; do echo $i; done;', function(err, out){
        var args = out.split("\n");
        var cmd = args.shift(), instances = 0;
        args.pop();
        function start() {
            try {
                var task = child_process.spawn(cmd, args, {cwd: process.env.PROJECTPATH,env: process.env});
                instances++;
                task
                    .on('exit', function() {
                        _this.pid = null;
                        if(restart) {
                            setTimeout(start, 1000);
                        }
                    })
                    .on('error', function() {
                        _this.pid = null;
                        console.log(arguments);
                    });
                task.stdout.on('data', function(data) {
                    console.log(name + ': ' + data.toString());
                });
                task.stderr.on('data', function(data) {
                    console.error(name + ': [err] ' + data.toString());
                });
                _this.pid = task.pid;
            } catch(e) {
                console.error(name + ': [taskman] ' + e.toString());
                if(instances == 0){
                    setTimeout(start, 5000);
                }
            }
        }
        start();
    });
}


Core.processNamespace(Taskman);

process.on('SIGINT', sighandler);
process.on('SIGTERM', sighandler);

function sighandler(){
    for(var i in Taskman.Server.lines){
        if(Taskman.Server.lines.hasOwnProperty(i)) {
            Taskman.Server.stopProcessesForLine(i);
        }
    }
    console.log('unwatch taskman.config');
    Taskman.Server.unwatchFile();
    Taskman.Server.server.close(function() {
        console.log('stopped socket server');
    });
    fs.unwatchFile(Taskman.revisionfile);
//    fs.unlink(Taskman.socketfile, function() {
//        console.log('deleted socketfile');
//    });
}
