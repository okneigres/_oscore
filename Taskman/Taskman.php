<?php

define('TASKMAN_CONFIG_FILE', getenv('PROJECTENV') . "/etc/taskman.conf");

class Taskman
{

    const port = 4545;

    const CONFIG_FILE = TASKMAN_CONFIG_FILE;

    static function installInCron()
    {
        CatchEvent(\_OS\Core\System_InitFiles::class);
        $current_dir = __DIR__;
        $crontab = `crontab -l`;
        $lines = explode("\n", $crontab);
        $installed = false;
        $install_string = "* * * * * source ~/.projectsrc; cdproject " . PROJECT . "; ./node " . PATH_WORKDIR . "/core/_OS/Taskman/Taskman.js 2>&1 >> " . PATH_LOG . "/taskman_daemon.log & # " . PROJECT . "/" . __METHOD__;
        $bash_installed = false;
        foreach ($lines as $i => $line) {
            if (preg_match("/# " . PROJECT . "\\/" . __METHOD__ . "/", $line)) {
                if (!$installed) {
                    $lines[$i] = $install_string;
                    $installed = true;
                } else {
                    $lines[$i] = '';
                }
            }
            if (preg_match("/SHELL=/", $line)) {
                if (trim($line) == 'SHELL=/bin/bash') {
                    $bash_installed = true;
                }
            }
        }
        if (!$installed) {
            $lines[] = $install_string;
        }
        if (!$bash_installed) {
            array_unshift($lines, "SHELL=/bin/bash");
        }
        if ($crontab != implode("\n", $lines)) {
            `mkdir -p  \$PROJECTENV/var/`;
            file_put_contents(PATH_ENV . "/var/crontab.bak", implode("\n", $lines) . "\n");
            passthru("crontab " . PATH_ENV . "/var/crontab.bak");
            unlink(PATH_ENV . "/var/crontab.bak");
        }
    }

    /**
     * Install task to run under Taskman
     *
     * @param string $daemon_name
     * @param string $cmd
     * @return void
     */
    static function installDaemonUnderTaskman($daemon_name, $cmd)
    {
        `mkdir -p \$PROJECTENV/etc`;
        `touch \$PROJECTENV/etc/taskman.conf`;
        $crontab = file_get_contents(Taskman::CONFIG_FILE);
        $lines = explode("\n", $crontab);
        $installed = false;
        $install_string = '* \'' . $daemon_name . '\' ' . $cmd;
        foreach ($lines as $i => $line) {
            if (preg_match("/[ \t]\'" . $daemon_name . "\'[ \t]/", $line)) {
                if ($installed)
                    continue;

                $lines[$i] = ($line[0] == '#' ? '#' : '') . $install_string;
                $installed = true;
            }
        }
        if (!$installed)
            $lines[] = $install_string;

        if ($crontab != implode("\n", $lines))
            file_put_contents(Taskman::CONFIG_FILE, implode("\n", $lines));
    }

    /**
     * Delete task from config file
     *
     * @static
     * @param string $daemon_name
     * @return void
     */
    static function deleteDaemonUnderTaskman($daemon_name)
    {
        $crontab = file_get_contents(Taskman::CONFIG_FILE);
        $lines = explode("\n", $crontab);

        foreach ($lines as $i => $line) {
            if (0 === strpos($line, '* \'' . $daemon_name . '\'') || !$line)
                unset($lines[$i]);
        }

        if ($crontab != implode("\n", $lines))
            file_put_contents(Taskman::CONFIG_FILE, implode("\n", $lines));
    }

    static function getRunningTasksByPattern($pattern)
    {
        $crontab = file_get_contents(Taskman::CONFIG_FILE);
        $lines = explode("\n", $crontab);

        $tasks = [];

        foreach ($lines as $i => $line) {
            if (strpos($line, $pattern))
                $tasks[] = $lines[$i];
        }
        return $tasks;
    }

    static function getPortFor($role, $instance = null)
    {
        $name = posix_getpwuid(posix_geteuid())["name"] . '-' . PROJECT . "-" . $role.(is_null($instance)?":".$instance:'');
        if (!file_exists(getenv("HOME") . "/projects-ports")) {
            file_put_contents(getenv("HOME") . "/projects-ports", '{}');
        }

        $rm = json_decode(file_get_contents(getenv("HOME") . "/projects-ports"), true);
        $rmall = [];

        foreach (glob("/home/*/projects-ports") as $file) {
            $rmall += json_decode(file_get_contents($file), true);
        }

        if (!isset($rmall[$name])) {
            $port = 44000;
            while (array_search($port, $rmall)) $port++;
            $rm[$name] = $port;
            file_put_contents(getenv("HOME") . "/projects-ports", JSON::hencode($rm));
        }
        return $rm[$name];
    }

    static function prepareConfigs($templates_dir, $dest_dir, $vars = []) {
        $vars += [
            "%PROJECT%"     => PROJECT,
            "%PROJECTENV%"  => PROJECTENV,
            "%PROJECTLOG%"  => PROJECTLOG,
            "%PROJECTDATA%" => PROJECTDATA,
            "%USER%"        => posix_getpwuid(posix_geteuid())["name"],
        ];
        `mkdir -p $dest_dir`;
        foreach(glob($templates_dir.'/*.template') as $file) {
            $filename = basename($file, ".template");
            file_put_contents($dest_dir."/".$filename,
                strtr(
                    file_get_contents($file),
                    $vars
                )
            );
        }

    }

}
