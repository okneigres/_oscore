var child_process = require('child_process');

var data = require('fs').readFile.sync(null, process.env.PROJECTDATA + '/roles2hosts.json');

var roles2hosts = JSON.parse(data);
var hosts2roles = {};

for (var role in roles2hosts) {
    if (!roles2hosts.hasOwnProperty(role)) {
        continue;
    }
    var hosts = roles2hosts[role];
    if (!(hosts instanceof Array)) hosts = [hosts];
    if (!hosts.length) {
        throw new Error('Not found hosts for role ' + role);
    }
    for (var j = 0; j < hosts.length; j++) {
        var host = hosts[j];
        if (!hosts2roles[host]) {
            hosts2roles[host] = [];
        }
        hosts2roles[host].push(role);
    }
}
var roles;
var futures = {};
for (host in hosts2roles) {
    if (!hosts2roles.hasOwnProperty(host)) {
        continue;
    }
    roles = hosts2roles[host];
    futures[host] = (function (host, roles, on_end) {
        console.log("\n===== deploy to " + host + " =====\n" + child_process.exec.sync(null, 'php-r \'Deploy::deployFromBase("' + host + '", ' + JSON.stringify(roles) + ');\''));
        on_end();
    }).future(null, host, roles);
}
for (host in hosts2roles) {
    if (!hosts2roles.hasOwnProperty(host)) {
        continue;
    }
    futures[host].result;
}
for (host in hosts2roles) {
    if (!hosts2roles.hasOwnProperty(host)) {
        continue;
    }
    roles = hosts2roles[host];
    (function (host, roles, on_end) {
        console.log("\n===== init data on " + host + " =====\n" + child_process.exec.sync(null, 'php-r \'Deploy::initData("' + host + '", ' + JSON.stringify(roles) + ');\''));
        on_end();
    }).future(null, host, roles);
}
console.log("node stage done");
