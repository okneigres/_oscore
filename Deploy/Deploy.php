<?php

class Deploy {

    static function deployBase($project) {
        $Project = Project::get($project);

        // проверяем коннект к базе
        passthru("ssh -x -o StrictHostKeyChecking=no {$Project->base} 'echo connected to base'", $retval);

        if($retval != 0) {
            throw new Exception("ssh {$Project->base} returned $retval for project \"{$Project->name}\"");
        }

        // копируем .projectsrc
        passthru("rsync ~/.projectsrc {$Project->base}:./", $retval);
        passthru("ssh -x -o StrictHostKeyChecking=no {$Project->base} 'grep projectsrc .bashrc || echo source ~/.projectsrc >> .bashrc'");

        // получаем номер ревизии (инкрементим). если папочки проекта и project.env нет, создаём
        passthru("ssh -x {$Project->base} 'mkdir -p ~/{$Project->name} ~/{$Project->name}.env ~/{$Project->name}.data'", $retval);
        echo 'oldrev=';
        $oldrev = system("ssh -x {$Project->base} 'cat ~/{$Project->name}/project-revision'");
        echo "\n";
        $newrev = date("md") . (substr($oldrev, 0, 4) != date("md") ? '000' : substr(1000 + floor(substr($oldrev, 4))+1, 1));

        echo "newrev=$newrev\n";

        if($oldrev) {
            // если есть старые файлы, скопируем их в папку новой ревизии
            passthru("ssh -x {$Project->base} 'cp -r ~/{$Project->name}/$oldrev ~/{$Project->name}/$newrev'", $retval);
        } else {
            // если нет, просто создадим папку
            passthru("ssh -x {$Project->base} 'mkdir -p ~/{$Project->name}/$newrev'", $retval);
        }


        // делаем rsync в эту папку
        passthru("rsync -Rr --delete --delete-excluded --exclude='tmp/*' --exclude='.git' --exclude='.idea' ./ {$Project->base}:{$Project->name}/$newrev");

        // запустим ./init_configs с ролью base
        passthru("ssh -x {$Project->base} 'source ~/.projectsrc; cdproject {$Project->name} $newrev; ROLES=\"BaseHost\" php core/_OS/Core/generate_maps.php'", $retval);
        passthru("ssh -x {$Project->base} 'source ~/.projectsrc; cdproject {$Project->name} $newrev; ROLES=\"BaseHost\" ./php-r \"System::InitFiles();\"'", $retval);
        passthru("ssh -x {$Project->base} 'source ~/.projectsrc; cdproject {$Project->name} $newrev; ROLES=\"BaseHost\" php core/_OS/Core/generate_maps.php'", $retval);
        passthru("ssh -x {$Project->base} 'source ~/.projectsrc; cdproject {$Project->name} $newrev; ROLES=\"BaseHost\" ./php-r \"System::InitConfigs();\"'", $retval);

        // создадим project-revision
        passthru("ssh -x {$Project->base} 'echo -n $newrev > ~/{$Project->name}/project-revision'", $retval);

        // выкладываемся с помощью nodejs на все перечисленные хосты
        passthru("ssh-agent bash -c \"ssh-add; ssh -o ForwardAgent=yes -x {$Project->base} 'source ~/.projectsrc; cdproject {$Project->name} $newrev; ROLES=\\\"BaseHost\\\" node core/_OS/Deploy/Deploy.js'\"", $retval);

    }

    static function deployFromBase($host, $roles) {
        sort($roles);
        echo "ROLES=\"".implode(" ", $roles)."\"\n";

        // проверяем коннект к хосту
        passthru("ssh -x -o StrictHostKeyChecking=no $host 'echo connected to $host'", $retval);
        passthru("ssh -x -o StrictHostKeyChecking=no $host 'grep projectsrc .bashrc || echo source ~/.projectsrc >> .bashrc'");

        if($retval != 0) {
            throw new Exception("ssh $host returned $retval for project \"".PROJECT."\"");
        }

        // копируем .projectsrc
        passthru("rsync ~/.projectsrc $host:./", $retval);

        // получаем номер ревизии (инкрементим). если папочки проекта и project.env нет, создаём
        passthru("ssh -x $host 'mkdir -p ~/".PROJECT." ~/".PROJECT.".env ~/".PROJECT.".data' ~/".PROJECT.".log'", $retval);
        passthru("ssh -x $host 'mkdir -p ~/".PROJECT.".env/var/ ~/".PROJECT.".env/etc/ ~/".PROJECT.".env/etc/nginx'", $retval);

        echo "oldrev=";
        $oldrev = system("ssh -x $host 'cat ~/".PROJECT."/project-revision'");
        echo "\n";
        $newrev = REVISION;
        echo "newrev=$newrev";
        echo "\n";

        if($oldrev) {
            if($newrev != $oldrev) {
                // если есть старые файлы, скопируем их в папку новой ревизии
                passthru("ssh -x $host 'cp -r ~/".PROJECT."/$oldrev ~/".PROJECT."/$newrev'", $retval);
            }
        } else {
            // если нет, просто создадим папку
            passthru("ssh -x $host 'mkdir -p ~/".PROJECT."/$newrev'", $retval);
        }

        // делаем rsync в эту папку
        passthru("rsync -Rr --delete ./ $host:".PROJECT."/$newrev");

        // копируем файл с ролями
        passthru("rsync ".PATH_DATA."/roles2hosts.json $host:".PROJECT.".data/");

        // копируем название своего хоста, как в конфиге
        passthru("ssh -x $host 'source ~/.projectsrc; cdproject ".PROJECT." $newrev; echo -n $host > \$PROJECTDATA/my.host'", $retval);

        // сохраним роли и запустим ./init_configs
        passthru("ssh -x $host 'source ~/.projectsrc; cdproject ".PROJECT." $newrev; ROLES=\"".implode(" ", $roles)."\" ./php-r \"System::InitConfigs();\"'", $retval);

        // создадим project-revision
        passthru("ssh -x $host 'echo -n $newrev > ~/".PROJECT."/project-revision'", $retval);

        // скопируем к себе ports config
        passthru("rsync $host:projects-ports ".PATH_DATA."/ports-$host.json");

    }

    static function initData($host, $roles) {
        passthru("rsync ".PATH_DATA."/ports-*.json $host:".PATH_DATA."/");
        passthru("ssh -x $host 'source ~/.projectsrc; cdproject ".PROJECT."; ROLES=\"".implode(" ", $roles)."\" ./php-r \"System::InitData();\"'", $retval);
    }

    static function deployFile($string) {
        list($project, $files) = explode(" ", $string, 2);
        $files = explode(" ", $files);
        $Project = Project::get($project);
        $roles2hosts = JSON::decode(`ssh {$Project->base} 'cat {$Project->name}.data/roles2hosts.json'`);
        $hosts = [];
        foreach($roles2hosts as $hs) {
            foreach($hs as $host) {
                if(!in_array($host, $hosts)) {
                    $hosts[] = $host;
                }
            }
        }
        $rev = `ssh {$Project->base} 'cat {$Project->name}/project-revision'`;

        list($user) = explode("@", $Project->base);

        foreach($files as $file) {
            foreach($hosts as $host) {
                $cmd = "scp $file $user@$host:$project/$rev/$file";
                echo $cmd."\n";
                passthru($cmd);
            }
        }
    }
}