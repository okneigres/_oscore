<?php

class cmp {

    static function exists()            { return new self(__FUNCTION__, null); }
    static function not_exists()        { return new self(__FUNCTION__, null); }
    static function more($val)          { return new self(__FUNCTION__, $val); }
    static function moreOrEq($val)      { return new self(__FUNCTION__, $val); }
    static function less($val)          { return new self(__FUNCTION__, $val); }
    static function lessOrEq($val)      { return new self(__FUNCTION__, $val); }
    static function eq($val)            { return new self(__FUNCTION__, $val); }
    static function not_eq($val)        { return new self(__FUNCTION__, $val); }
    static function startsWith($val)    { return new self(__FUNCTION__, $val); }

    var $method;
    var $value;

    function __construct($method, $value) {
        $this->method = $method;
        $this->value  = $value;
    }

    function check($sample) {
        switch($this->method) {
            case 'exists';
                return !empty($sample);
            case 'not_exists';
                return empty($sample);
            case 'more';
                return $this->value > $sample;
            case 'moreOrEq';
                return $this->value >= $sample;
            case 'less';
                return $this->value < $sample;
            case 'lessOrEq';
                return $this->value <= $sample;
            case 'eq';
                return $this->value == $sample;
            case 'not_eq';
                return $this->value != $sample;
            case 'startsWith';
                return is_string($sample) && substr($sample, 0, strlen($this->value)) == $this->value;
        }
    }

}