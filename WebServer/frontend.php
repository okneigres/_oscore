<?php

require_once('../Core/preinit.php');

putenv('PROJECT=' . PROJECT);
putenv('PROJECTENV=' . PATH_ENV);
putenv('PROJECTLOG=' . PATH_LOG);
putenv('PROJECTPATH=' . PATH_WORKDIR);
putenv('PROJECTREV=' . REVISION);
putenv('PATH=' . PATH_WORKDIR . ':' . PATH_ENV . '/bin:' . getenv('PATH'));
putenv('IS_FPM_MODE=1');

if (!empty($_SERVER['PHP_CMD'])) {
    (new WebContext)->run(function () {
        try {
            WorkSession::start();
            call_user_func($_SERVER['PHP_CMD']);
        } finally {
            WorkSession::end();
        }
    });
} elseif (!empty($_SERVER['PHP_FILE'])) {
    (new WebContext)->run(function () {
        try {
            WorkSession::start();
            require_once $_ENV['PHP_FILE'];
        } finally {
            WorkSession::end();
        }
    });
} elseif (isset($_SERVER['DOCUMENT_URI'])) {
    (new WebContext)->run(function () {
        try {
            WorkSession::start();
            require_once $_SERVER['DOCUMENT_ROOT'] . "/" . $_SERVER['DOCUMENT_URI'];
        } finally {
            WorkSession::end();
        }
    });
} else {
    throw new Exception("Don't know how to handle request, globals: " . JSON::hencode($GLOBALS));
}