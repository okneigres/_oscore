<?php

abstract class NginxFrontend {

    static function getSiteNames() {
        throw new Exception('You need to create method '.get_called_class().'::'.__FUNCTION__);
    }

    static function generateRoles() {
        CatchEvent(\_OS\Core\System_InitFiles::class);

        foreach([static::class.'NginxBalancer', static::class.'NginxApplication'] as $role) {
            file_put_contents(PATH_WORKDIR."/tmp/$role.php",
<<<EOF
<?php

class $role extends HostRole {

}


EOF
            );
        }
    }

    static function configureNginxBalancer() {
        CatchEvent([__CLASS__.'NginxBalancer' => [\_OS\Core\System_InitConfigs::class]]);

        $NginxConfig = new NginxConfig();

        $upstreams = HostRole::getRoleHosts(get_called_class(). 'NginxApplication', false, true);

        $upstream_name = posix_getpwuid(posix_geteuid())["name"]."-".PROJECT.'-'.get_called_class(). 'NginxApplication';

        $NginxConfig->definitions['upstream'] = "
            upstream $upstream_name { ";

        foreach($upstreams as $upstream) {
            $NginxConfig->definitions['upstream'] .= "
                server $upstream:80 fail_timeout=0;
            ";
        }

        $NginxConfig->definitions['upstream'] .= "
            }
        ";

        $rev = REVISION?:"000";

        $NginxConfig->sections['location /'] = "
            location / {
                proxy_pass http://$upstream_name;
                proxy_set_header Host $rev.\$http_host;
            }
        ";

        $sites = static::getSiteNames();

        $nginx_config = strtr(file_get_contents(__DIR__."/nginx.conf.template"), array(
            '%PROJECT%'             => PROJECT,
            '%ROLE%'                => 'balancer',
            '%PROJECTPATH%'         => PATH_WORKDIR,
            '%PATH_ENV%'            => PATH_ENV,
            '%PATH_LOG%'            => PATH_LOG,
            '%__DIR__%'             => __DIR__,
            '%Ymd%'                 => date("Ymd"),
            '%USER%'                => posix_getpwuid(posix_geteuid())["name"],
            '%DEFINITIONS%'         => implode("\n", $NginxConfig->definitions),
            '%SECTIONS%'            => implode("\n", $NginxConfig->sections),
            '%SERVER_NAME%'         => implode(" ", array_map(function($domain){ return idn_to_ascii($domain);}, $sites)),
        ));

        $nginx_config = strtr($nginx_config, array(
            '%PHP_FPM%'             => ''
        ));

        file_put_contents(PATH_ENV.'/etc/nginx/nginx-'.static::class.'Balancer.conf', self::formatConfig($nginx_config));
        self::restartNginx();
        Taskman::installDaemonUnderTaskman('nginx-logger',         './php-r "LogStashWriter::fromStdin(null, [\\"source\\" => \\"nginx\\"     , \\"type\\" => \\"info\\"],  \\"tail -fn 0 $PROJECTENV/var/nginx-access.log\\");"');
        Taskman::installDaemonUnderTaskman('nginx-logger-error',   './php-r "LogStashWriter::fromStdin(null, [\\"source\\" => \\"nginx\\"     , \\"type\\" => \\"error\\"], \\"tail -fn 0 $PROJECTLOG/'.date("Ymd").'-error-nginx.log\\");"');
    }

    static function formatConfig($nginx_config) {
        $level = 1;
        $formatted_config = [];

        foreach(explode("\n", $nginx_config) as $line) {
            $line = trim($line);
            if(strpos($line, '}') !== false) $level--;
            $formatted_config[] = str_pad("", 4*$level, " ").$line;
            if(strpos($line, '{') !== false) $level++;
        }

        return join("\n", $formatted_config);
    }

    static function restartNginx() {
        _OSTask::run_inline(array(
            'is_possible' => function(Context $Ctx) {
                    passthru('/usr/sbin/nginx -v', $ret);
                    return $ret == 0;
                },
            'make_possible' => function(Context $Ctx) {
                    passthru('echo PLEASE, INSTALL nginx AND MAKE POSSIBLE sudo /usr/sbin/nginx TO DEPLOY SCRIPT; sleep 60', $ret);
                },
            'been_run' => function(Context $Ctx) {
                    return isset($Ctx->been_run);
                },
            'run' => function(Context $Ctx) {
                    passthru('sudo /etc/init.d/nginx configtest', $ret);

                    if($ret == 0){
                        passthru('sudo /etc/init.d/nginx status || sudo /etc/init.d/nginx start ; sudo /etc/init.d/nginx reload');
                    } else {
                        throw new Exception("Nginx test failed");
                    }
                    $Ctx->been_run = true;
                }
        ));
    }

    static function configureNginxApplication() {
        CatchEvent([static::class.'NginxApplication' => \_OS\Core\System_InitConfigs::class]);

        passthru('mkdir -p $PROJECTENV/etc/php $PROJECTENV/etc/nginx');

        $NginxConfig = new NginxConfig();

        $NginxConfig->sections['location /core'] = "
            location /core {
                root ".PATH_WORKDIR.";
            }
        ";

        $NginxConfig->sections['location /favicon.ico'] = "
            location = /favicon.ico {
                root ".__DIR__.";
            }
        ";

        (new \Event\_OS\WebServer\GenerateNginxConfig($NginxConfig))->dispatch();

        $rev = REVISION?:"000";

        $sites = static::getSiteNames();

        $nginx_config = strtr(file_get_contents(__DIR__."/nginx.conf.template"), array(
            '%PROJECT%'             => PROJECT,
            '%ROLE%'                => $rev,
            '%PROJECTPATH%'         => PATH_WORKDIR,
            '%PATH_ENV%'            => PATH_ENV,
            '%PATH_LOG%'            => PATH_LOG,
            '%__DIR__%'             => __DIR__,
            '%Ymd%'                 => date("Ymd"),
            '%USER%'                => posix_getpwuid(posix_geteuid())["name"],
            '%DEFINITIONS%'         => implode("\n", $NginxConfig->definitions),
            '%SECTIONS%'            => implode("\n", $NginxConfig->sections),
            '%SERVER_NAME%'         => implode(" ", array_map(function($domain) use ($rev) { return $rev.".".idn_to_ascii($domain);}, $sites)),
        ));

        $nginx_config = strtr($nginx_config, array(
            '%PHP_FPM%'             => '
                fastcgi_param   QUERY_STRING            $query_string;
                fastcgi_param   REQUEST_METHOD          $request_method;
                fastcgi_param   CONTENT_TYPE            $content_type;
                fastcgi_param   CONTENT_LENGTH          $content_length;
                fastcgi_param   SCRIPT_FILENAME         $request_filename;
                fastcgi_param   SCRIPT_NAME             $fastcgi_script_name;
                fastcgi_param   REQUEST_URI             $request_uri;
                fastcgi_param   DOCUMENT_URI            $document_uri;
                fastcgi_param   DOCUMENT_ROOT           $document_root;
                fastcgi_param   SERVER_PROTOCOL         $server_protocol;
                fastcgi_param   GATEWAY_INTERFACE       CGI/1.1;
                fastcgi_param   SERVER_SOFTWARE         nginx/$nginx_version;
                fastcgi_param   REMOTE_ADDR             $remote_addr;
                fastcgi_param   REMOTE_PORT             $remote_port;
                fastcgi_param   SERVER_ADDR             $server_addr;
                fastcgi_param   SERVER_PORT             $server_port;
                fastcgi_param   SERVER_NAME             $server_name;
                fastcgi_param   PHP_CMD                 $php_cmd;
                fastcgi_param   PHP_FILE                $php_file;

                fastcgi_param   HTTPS                   $https;

                # PHP only, required if PHP was built with --enable-force-cgi-redirect
                fastcgi_param   REDIRECT_STATUS         200;
                fastcgi_param   SCRIPT_FILENAME         '.__DIR__.'/frontend.php;
                fastcgi_param   SCRIPT_NAME             '.__DIR__.'/frontend.php;

                set $php_fpm unix:'.PATH_ENV.'/var/php-fpm.sock;
            '
        ));

        file_put_contents(PATH_ENV.'/etc/nginx/nginx-'.static::class.'.'.$rev.'.conf', self::formatConfig($nginx_config));
        self::restartNginx();
    }

    static function configureFpm() {
        CatchEvent([static::class.'NginxApplication' => \_OS\Core\System_InitConfigs::class]);

        $sites = static::getSiteNames();

        $fpm_config = strtr(file_get_contents(__DIR__."/php-fpm.conf.template"), array(
            '%PROJECTPATH%'         => PATH_WORKDIR,
            '%PATH_ENV%'            => PATH_ENV,
            '%PATH_LOG%'            => PATH_LOG,
            '%__DIR__%'             => __DIR__,
            '%Ymd%'                 => date("Ymd"),
            '%SERVER_NAME%'         => implode(" ", array_map(function($domain){ return idn_to_ascii($domain); }, $sites)),
        ));

        `mkdir -p \$PROJECTENV/etc/php/`;
        file_put_contents(PATH_ENV."/etc/php/php-fpm.conf", $fpm_config);

        $fpm_config = strtr(file_get_contents(__DIR__."/php-fpm.pool.conf.template"), array(
            '%PROJECT%'             => getenv('PROJECT'),
            '%PROJECTENV%'          => getenv('PROJECTENV'),
            '%PROJECTLOG%'          => getenv('PROJECTLOG'),
            '%PROJECTPATH%'         => getenv('PROJECTPATH'),
            '%PROJECTREV%'          => getenv('PROJECTREV'),
            '%REVCOMMENT%'          => getenv('PROJECTREV')?'':';',
            '%PATH%'                => getenv('PATH'),
            '%PATH_ENV%'            => PATH_ENV,
            '%PATH_LOG%'            => PATH_LOG,
            '%get_current_user()%'  => get_current_user(),
            '%get_current_group()%' => '',
            '%__DIR__%'             => __DIR__,
            '%SERVER_NAME%'         => implode(" ", array_map(function($domain){ return idn_to_ascii($domain);}, $sites)),
        ));

        file_put_contents(PATH_ENV."/etc/php/php-fpm.pool.conf", $fpm_config);

        $fpm_config = strtr(file_get_contents(__DIR__."/php.ini.template"), array(
            '%PROJECTPATH%'         => PATH_WORKDIR,
            '%PROJECTENV%'          => PATH_ENV,
            '%PATH_ENV%'            => PATH_ENV,
            '%PATH_LOG%'            => PATH_LOG,
            '%__DIR__%'             => __DIR__,
            '%SERVER_NAME%'         => implode(" ", array_map(function($domain){ return idn_to_ascii($domain);}, $sites)),
        ));

        file_put_contents(PATH_ENV."/etc/php/php.ini", $fpm_config);

        Taskman::installDaemonUnderTaskman('php-fpm', '/usr/sbin/php5-fpm -F -g $PROJECTENV/var/php-fpm.pid -y $PROJECTENV/etc/php/php-fpm.conf -c $PROJECTENV/var/php.ini');
        Taskman::installDaemonUnderTaskman('php-fpm-logger-error', './php-r "LogStashWriter::fromStdin(null, [\\"source\\" => \\"fpm-error\\" , \\"type\\" => \\"error\\"], \\"tail -fn 0 $PROJECTLOG/'.date("Ymd").'-info-fpm.log\\");"');
        Taskman::installDaemonUnderTaskman('php-fpm-logger-slow',  './php-r "LogStashWriter::fromStdin(null, [\\"source\\" => \\"fpm-slow\\"  , \\"type\\" => \\"warn\\" ], \\"tail -fn 0 $PROJECTLOG/fpm-slow.log\\");"');
    }

}
