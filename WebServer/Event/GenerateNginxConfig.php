<?php

namespace Event\_OS\WebServer;


class GenerateNginxConfig extends \_OS\Event {

    const SEND_TO_LOGGER = false;

    public $NginxConfig;

    function __construct(\NginxConfig $NginxConfig) {
        $this->NginxConfig = $NginxConfig;
    }

}