<?php

namespace _OS;


abstract class Event {

    const SEND_TO_LOGGER = true;

    function __construct() {
    }

    function dispatch() {
        \_OS\CoreEvents::dispatchEvent($this);
    }

}