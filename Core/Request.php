<?php

namespace _OS;


abstract class Request {

    public $result;

    function dispatch($target = null) {
        return \_OS\CoreRequests::dispatchRequest($this, $target);
    }

}