<?php
namespace _OS;

class CoreRequests {

    static $requestsMap = array();
    static $requestsMapIsActual = false;

    const REQUESTS_MAP_DIR = './';
    const REQUESTS_MAP_FILE = 'tmp/php_request_map.json';

    static $LastCatchedRequests = array();

    static function preinit() {
        self::$requestsMap = json_decode(file_get_contents(self::REQUESTS_MAP_DIR. "/". self::REQUESTS_MAP_FILE), true);
    }

    static function dispatchRequest(\_OS\Request $request, $target = null) {

        if(isset($target)) {
            self::$LastCatchedRequests[] = $request;
            $result = call_user_func($target);
            array_pop(self::$LastCatchedRequests);
            if(isset($result)) {
                return $result;
            } else {
                throw new \Exception("Cannot execute request ".get_class($request)." - there is no valid handlers");
            }
        }

        if(isset(self::$requestsMap[$classname = get_class($request)])) {
            self::$LastCatchedRequests[] = $request;
            foreach(self::$requestsMap[$classname] as $method) {
                $result = call_user_func($method);
                if(!is_null($result)) {
                    $request->result = $result;
                }
                if(!is_null($request->result)){
                    break;
                }
            }
            array_pop(self::$LastCatchedRequests);
            if(!is_null($request->result)) {
                return $request->result;
            } else {
                throw new \Exception("Cannot execute request ".get_class($request)." - there is no valid handlers");
            }
        }
        throw new \Exception("Cannot execute request ".get_class($request)." - no handlers registered");
    }

    static function generateMap() {

        $request2listeners = array();

        $files = explode("\n", trim(`find ./ | grep .php`));
        sort($files);

        foreach($files as $file) {
            $parts = explode('/', substr($file,0,-4));
            $class = end($parts);
            $file_contents = file_get_contents($file);

            if(preg_match("/namespace +([a-zA-Z0-9\\\\]+)/", $file_contents, $matches)) {
                $class = "\\".$matches[1]."\\".$class;
            } else {
                $class = "\\".$class;
            }

            if($class=="Requests")
                continue;

            if(class_exists($class)) {
                $rc = new \ReflectionClass($class);
                if($rc->isAbstract()) {
                    continue;
                }
                $traits = $rc->getTraits();
                foreach($rc->getMethods(\ReflectionMethod::IS_STATIC) as  $rm) {
                    $strings = explode("\n", $file_contents);
                    $strings = array_slice($strings, $rm->getStartLine(), $rm->getEndLine() - $rm->getStartLine());
                    $strings = implode("\n",$strings);
                    preg_match('/CatchRequest\\(([^\\)]+)\\);/', $strings, $matches);
                    if(!isset($matches[1])){
                        continue;
                    }
                    $requests = trim($matches[1],"\n\r ,");
                    /**
                     * @var $requests array
                     */
                    eval('$requests = array('.$requests.');');
                    if(!$requests){
                        continue;
                    }
                    $traitMethod = false;
                    foreach ($traits as $trait) {
                        // If the trait has a method, that's it!
                        if ($trait->hasMethod($rm->getName())) {
                            $traitMethod = true;
                        }
                    }
                    if ($traitMethod) continue;
                    $method_name = $rm->getDeclaringClass()->getName()."::".$rm->getName();
                    foreach($requests as $request) {
                        if(!is_subclass_of($request, \_OS\Request::class)) {
                            throw new \Exception("Shit happens: $request is not Request class for method ".$method_name);
                        }
                        if(!isset($request2listeners[$request])) {
                            $request2listeners[$request] = array();
                        }
                        $request2listeners[$request][] = $method_name;
                    }
                }
            }
        }
        file_put_contents(self::REQUESTS_MAP_DIR. "/". self::REQUESTS_MAP_FILE, json_encode($request2listeners, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
    }

}