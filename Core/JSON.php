<?php

class JSON {

    static function encode($val) {
        return json_encode($val, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    static function decode($val) {
        return json_decode($val, true);
    }

    static function hencode($val) {
        return json_encode($val, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

}