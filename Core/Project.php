<?php

class Project {

    var $name;
    var $base;
    var $domain;

    function __construct($name) {

        $this->name = $name;
        $this->base = self::getConfig($name)['base'];
        $this->domain = self::getConfig($name)['domain'];
    }

    public static function get($name) {
        return new self($name);
    }

    public static function getCurrent() {
        return new self(PROJECT);
    }

    public static function getConfig($name = PROJECT) {
        if(!file_exists(PATH_WORKDIR.'/domain/projects.json')) {
            throw new Exception('domain/projects.json not found');
        }
        $configs = JSON::decode(file_get_contents(PATH_WORKDIR.'/domain/projects.json'));
        if(!$config = isset($configs[$name])?$configs[$name]:$configs['*']) {
            throw new Exception("Project \"$name\" is not found in domain/projects.json");
        }
        return $config;
    }

}