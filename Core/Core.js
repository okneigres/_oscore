if(typeof window != 'undefined') {
    global = window;
}

try {
    global.__defineSetter__('__inline_debug__', function(value) {
        console.error('inline debugger reports:', value);
    });
} catch(e) {
    // игнорируем - defineSetter не поддерживается
}

var Core = {
    __event_stack: [],
    _eventsTracking: 0,
    _eventsTrackingObjects: [],
    EnableEventsTracking: function(object) {
        if(object) {
            this._eventsTrackingObjects.push(object);
        } else {
            this._eventsTracking++;
        }
    },
    EventPoint: function() {
        function event(data) {
            var i;
            if(arguments.length > 1) {
                console.error('we does not support events with big number of arguments. only 1 or nothing.');
            }
            if(data) {
                for(i in data) {
                    this[i] = data[i];
                }
            }
            if(Core._eventsTracking) {
                console.log(event._name)
            }
            for(i in event.listeners) {
                Core.__event_stack.unshift(this);
                if(Core._eventsTrackingObjects.length && Core._eventsTrackingObjects.indexOf(event.listeners[i][0]) !== -1){
                    console.log(event._name + ': [object].' + event.listeners[i][1]);
                    Core._eventsTracking ++;
                }
//                try {
                event.listeners[i][0][event.listeners[i][1]](arguments[0]);
//                } catch (e) {
//                    console.error(e);
//                }
                if(Core._eventsTrackingObjects.length && Core._eventsTrackingObjects.indexOf(event.listeners[i][0]) !== -1){
                    Core._eventsTracking --;
                }
                Core.__event_stack.shift(this);
            }
        }
        event.listeners = [];
        return event;
    },
    RequestPoint: function() {
        return new Core.EventPoint();
    },
    _namespaces: {},
    getNamespace: function(namespace) {
        if(!this._namespaces[namespace]) {
            this._namespaces[namespace] = new function(){
                this.processNamespace = function() {
                    Core.processNamespace(this);
                }
            };
        }
        return this._namespaces[namespace];
    },
    processNamespace: function(namespace) {
        for(var _classname in namespace) {
            var _class = namespace[_classname];
            if (_class.__inited__)
                continue;
            if (_class.__init instanceof Function) {
                _class.__init();
            }
            for(var method in _class) {
                var events;
                if (_class[method] instanceof Function) {
                    if (events = _class[method].toString().replace(/\n/g,"").match(/(Core\.)?(CatchEvent|CatchRequest)\(([^\)]+)\)/m)) {
                        events = events[3].replace(/^ *| *$/g,"").split(/ *, */);
                        for(var i in events) {
                            try {
                                var parts = events[i].split('.');
                                var cursor = global;
                                for(var n in parts) {
                                    cursor = cursor[parts[n]];
                                }
                                cursor.listeners.push([_class, method]);
                                cursor._name = events[i];
                            } catch(e) {
                                console.error('cannot parse ' + events[i] + ' in CatchEvent in [namespace].' + _classname + '.' + method, e);
                            }
                        }
                    }
                }
            }
            if(_class.Init) {
                try {
                    new _class.Init;
                } catch(e) {
                    console.error(e);
                }
            }
            _class.__inited__ = true;
        }
    },
    processObject: function(object) {
        var _class = object;
        if (_class.__inited__)
            return;
        if (_class.__init instanceof Function) {
            _class.__init();
        }
        for(var method in _class) {
            var events;
            if (_class[method] instanceof Function) {
                if (events = _class[method].toString().replace(/\n/g,"").match(/(Core\.)?(CatchEvent|CatchRequest)\(([^\)]+)\)/m)) {
                    events = events[3].replace(/^ *| *$/g,"").split(/ *, */);
                    for(var i in events) {
                        try {
                            var parts = events[i].split('.');
                            var cursor = global;
                            for(var n in parts) {
                                cursor = cursor[parts[n]];
                            }
                            cursor.listeners.push([_class, method]);
                            cursor._name = events[i];
                        } catch(e) {
                            console.error('cannot parse ' + events[i] + ' in CatchEvent in [namespace].' + '.' + method, e);
                        }
                    }
                }
            }
        }
        if(_class.Init) {
            try {
                new _class.Init;
            } catch(e) {
                console.error(e);
            }
        }
        _class.__inited__ = true;
    },
    CatchEvent: function() { return Core.__event_stack[0]; /* supress no arguments warning */ arguments;},
    CatchRequest: function() { return Core.__event_stack[0]; /* supress no arguments warning */ arguments;},
    Env: function() {
        var args = arguments;
        return function(_class, pattern) {
            for(var i = 0; i < args.length; i++) {
                for(var j in args[i]) {
                    if(args[i][j] instanceof _class) {
                        if(!pattern)
                            return args[i][j];
                        var ret = true;
                        for(var k in pattern) {
                            if(args[i][j][k] != pattern[k]) {
                                ret = false;
                                break;
                            }
                        }
                        if(ret) {
                            return args[i][j];
                        }
                    }
                }
            }
            return null;
        }
    }
};

if(typeof window != 'undefined') {

    Event.DOM = {
        Init: new Core.EventPoint()
    };

    Event.Window = {
        Scroll: new Core.EventPoint(),
        Resize: new Core.EventPoint()
    };

    // cross-browser DOM_Ready
    (function contentLoaded(win, fn) {

        var done = false, top = true,

            doc = win.document, root = doc.documentElement,

            add = doc.addEventListener ? 'addEventListener' : 'attachEvent',
            rem = doc.addEventListener ? 'removeEventListener' : 'detachEvent',
            pre = doc.addEventListener ? '' : 'on',

            init = function(e) {
                if (e.type == 'readystatechange' && doc.readyState != 'complete') return;
                (e.type == 'load' ? win : doc)[rem](pre + e.type, init, false);
                if (!done && (done = true)) fn.call(win, e.type || e);
            },

            poll = function() {
                try { root.doScroll('left'); } catch(e) { setTimeout(poll, 50); return; }
                init('poll');
            };

        if (doc.readyState == 'complete') fn.call(win, 'lazy');
        else {
            if (doc.createEventObject && root.doScroll) {
                try { top = !win.frameElement; } catch(e) { }
                if (top) poll();
            }
            doc[add](pre + 'DOMContentLoaded', init, false);
            doc[add](pre + 'readystatechange', init, false);
            win[add](pre + 'load', init, false);
        }

    })(window, function(e){
        var old_onscroll, old_onresize;
        if(window.onscroll || document.body.onscroll) {
            old_onscroll = window.onscroll || document.body.onscroll;
        }
        if(window.onresize || document.body.onresize) {
            old_onresize = window.onresize || document.body.onresize;
        }

        new Event.DOM.Init(e);
        window.onscroll = document.body.onscroll = function(event) {
            if(old_onscroll) {
                old_onscroll(event);
            }
            new Event.Window.Scroll(event);
        };
        window.onresize = document.body.onresize = function(event) {
            if(old_onresize) {
                old_onresize(event);
            }
            new Event.Window.Resize(event);
        };

    });
}

if(typeof global.Event == 'undefined' ) {
    global.Event = {};
}

if(typeof require != 'undefined') {
    module.exports = Core;
}

CatchEvent = function(){ return Core.CatchEvent.apply(Core, arguments); };
CatchRequest = function(){ return Core.CatchRequest.apply(Core, arguments); };
EventPoint = Core.EventPoint;
