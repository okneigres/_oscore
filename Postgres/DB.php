<?php

class DB {

    const secret = "mcwo98y4hbkczkwowjlroj39r83";

    static $connection = false;

    static $adapter = 'pgsql';
    static $host = 'localhost';
    static $db_name = 'karaoke_widget';
    static $user = 'karaoke_widget';
    static $pass = '123321';

    /** @var PDO */
    protected $db_link;

    /** @return PDO */
    static function connect($dsn = null) {
        if (!self::$connection) {
            self::$connection = new self();
            self::$connection->db_link = self::storage($dsn);
        }
        return self::$connection->db_link;
    }

    static function storage($dsn = null) {
        if(!$dsn) {
            $dsn = self::$adapter .
                ':host=' . HostRole::getRoleHosts(MapperPostgresHost::class, true, true) .
                ';port=' . Taskman::getPortFor(MapperPostgresHost::class).
                ';dbname=' . __PROJECT__ .
                ';user= ' . __PROJECT__ .
                ';password=' .md5(DB::secret.__PROJECT__);
        }

        $pdo = new PDO($dsn);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    }

//    function __call($method, $args) {
//        $result = call_user_func_array([$this->db_link, $method], $args);
////        $error_info = $this->db_link->errorInfo()[2];
////        if($error_info) {
////            throw new Exception($error_info);
////        }
////        if($result instanceof PDOStatement) {
////            $result = new UtilDecorator($result, function($method, $args) {
////                /** @var PDOStatement $this */
////                $result = call_user_func_array([dbg::$i->sdij = $this, $method], $args);
//////                dbg::$i->sjo = $this->errorInfo();
//////                $error_info = $this->errorInfo()[2];
//////                if($error_info) {
//////                    throw new Exception($error_info);
//////                }
////                return $result;
////            });
////        }
//        return $result;
//    }

}