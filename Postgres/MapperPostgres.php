<?php

trait MapperPostgres {

    protected $_just_created;

    static function getList($filters = []) {
        $ph = DB::connect(isset(self::$external_dsn)?self::$external_dsn:null)->prepare('select * from '.static::$table.($filters?' where '
            .implode(
                ' and ',
                array_map(
                    function($field) {
                        return "$field = :$field";
                    },
                    array_keys($filters)
                )
            ):'')
        );
        $params = [];
        foreach($filters as $key=>$val) {
            $params[":$key"] = $val;
        }
        $ph->execute($params);
        $list = [];
        foreach($ph->fetchAll() as $fields) {
            $o = new self();
            $o->id   = $fields['id'];
            $o->data = JSON::decode($fields['data']);
            $list[] = $o;
        }
        return $list;
    }

    /**
    * Get indexed fields with its values
    *
    * @access protected
    * @return array
    */
    protected function getIndexedData() {
        $indexed = [];
        foreach (static::$fields as $field => $params) {
            if (!isset($params['index']))
                continue;

            if (isset($this->data[$field]))
                $indexed[$field] = $this->data[$field];
            elseif (isset($params['default']))
                $indexed[$field] = $params['default'];
        }
        
        return $indexed;
    }

    /**
    * Save the data in database
    *
    * @return $this
    */
    function save() {      
        $data = JSON::encode($this->data);        
        $all_fields = ['id' => $this->id] + $this->getIndexedData() + ['data' => $data];
        if($this->_just_created) {
            $ps = DB::connect(isset(self::$external_dsn)?self::$external_dsn:null)->prepare("
                insert into ".static::$table."(".implode(",", array_keys($all_fields)).")
                values (".implode(", ", array_map(function($field) {return ":$field";}, array_keys($all_fields))).")
            ");                     
        } else {
            $ps = DB::connect(isset(self::$external_dsn)?self::$external_dsn:null)->prepare("
                update ".static::$table."
                set ".implode(", ", array_map(function($field) {return "$field = :$field";}, array_keys($all_fields)))."
                where id = :id
            ");
        }
        $ps->execute($all_fields);
        $this->_just_created = false;

        return $this;
    }

    /** @return static */
    static function getById($id) {
        $object = static::getList(['id' => $id]);
        return isset($object[0]) ? $object[0] : null;
    }

    /**
    * Increment counters in database
    *
    * @param array $counters
    * @return $this
    */
    public function increment(array $counters) {
        if (!$counters) {
            Logger()->error('Empty counters for increment value in database', __CLASS__);
            return;
        }

        // Fetch data if
        if (!$this->data) {
            if ($Obj = self::getById($this->id))
                $this->data = $Obj->getData();
            else
                return;
        }

        // Increment data keys
        foreach ($this->data as $key => $val) {
            if (isset($counters[$key]))
                $this->data[$key] += $counters[$key];
        }

        return $this->save();
    }
    /**
     * Запись (создание или обновление) строки в таблице
     * Если в классе определено свойство static array $counter_keys,
     * то указанные ключи обновляются как счетчики key = key + value (происходит инкремент)
     * 
     * @param array $fields
     * @return $this
     */
    public function store(array $fields) {
        if ($Obj = self::getById($this->id)) {
            $data           = $Obj->getData();
            $has_counters   = property_exists(__CLASS__, 'counter_keys') ;
            foreach ($fields as $key => $field) {
                $data[$key] = $has_counters && in_array($key, self::$counter_keys) 
                            ? $data[$key] + $field 
                            : $field;
            }
            $this->data = array_merge($Obj->data, $data);
        } else {
            $this->_just_created = true;
            $fields['id'] = $this->id;
            // дописываем неопределенные индексы
            foreach (self::$fields as $key => $field) {
                if (!isset($fields[$key]))
                    $fields[$key] = $field['default'];
            }
            $this->data = $fields;
        }
        return $this->save();
    }
    
    /**
     * Генерация таблицы в базе данных, соответствующей модели, если таблица не создана, создается
     *
     * @todo Modify index declaration in database if code changes
     * @uses DB
     * @throws Exception
     * @return void
     */
    static function generateTable() {
        CatchEvent([__TRAIT__."Host" => System_InitConfigs]);

        // Карта соответствий объявляемых типов и создания в базе
        static $type_map = [
            'int'       => 'integer',
            'float'     => 'float',
            'double'    => 'float',
            'string'    => 'text',
            'date'      => 'int',
            'bool'      => 'boolean',
            'blob'      => 'text',
        ];

        $indexes = [];

        $conn = DB::connect(isset(self::$external_dsn)?self::$external_dsn:null);
        $sql = 'CREATE TABLE IF NOT EXISTS ' . self::$table . ' (';
        foreach (self::$fields as $field => $params) {
            // Если нет индекса
            if (!array_key_exists('index', $params) || !$params['index'])
                continue;

            // Удостоверимя. что в карте есть соответствие
            if (!isset($type_map[$params['type']]))
                throw new Exception('Undefined type in Postgres Type Map ' . $params['type'] . ' in class ' . self::class);

            $sql .= $field . ' ' . $type_map[$params['type']] . ' NOT NULL DEFAULT ' . (isset($params['default']) ? $conn->quote($params['default']) : 'NULL') . ',';

            // Если для этого поля нужно создать индекс, выбираем какой из индексов необходимо добавить
            $row = &$indexes[$field];
            switch ((string) $params['index']) {
                case 'primary':
                    $row = 'ALTER TABLE ' . self::$table . ' ADD PRIMARY KEY (' . $field . ')';
                    break;
                case 'unique':
                    $row = 'CREATE UNIQUE INDEX ON ' . self::$table . ' (' . $field . ')';
                    break;
                default:
                    $row = 'CREATE INDEX ON ' . self::$table . ' (' . $field . ');';
                    break;
            }
        }
        $sql .= 'data ' . $type_map['blob'] . ' NOT NULL DEFAULT \'\');';               
        
        // Creating talbe if it doesn't exist
        $conn->query($sql);
                  
        // Getting all indexes for current table        
        $stmt = $conn->prepare('
            SELECT a.attname AS column_name 
            FROM pg_class t, pg_class i, pg_index ix, pg_attribute a
            WHERE t.oid = ix.indrelid AND i.oid = ix.indexrelid AND a.attnum = ANY(ix.indkey) AND t.relname = :table
        ');        
        $stmt->execute([':table' => self::$table]);
        $current_indexes = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);        
    
        // Creating undefined indexes for table that doesn't exist
        foreach ($indexes as $name => $sql) {
            if (in_array($name, $current_indexes))
                continue;

            $conn->query($sql);
        }

    }

    static function _PostgresStorageMakeMasterRole() {
        CatchEvent(System_InitConfigs);

        if(isset(self::$external_dsn)) {
            return;
        }

        $RoleClass = __TRAIT__."Host";
        $__DIR__   = __DIR__;
        $port      = Taskman::getPortFor($RoleClass);

        $template = <<<EOF
<?php

class $RoleClass extends HostRole {

    static function _PostgresBaseDaemon() {
        CatchEvent([$RoleClass::class => System_InitConfigs]);
        \$pgdir = glob('/usr/lib/postgresql/*');
        \$pgdir = end(\$pgdir);

        if(!\$pgdir) {
            throw new Exception("Postgresql not found");
        }

        \$confdir = PROJECTENV.'/etc/postgres/';
        passthru("mkdir -p \$confdir");

        Taskman::prepareConfigs('$__DIR__/etc/', \$confdir, [
            '%PORT%' => $port
        ]);

        Taskman::installDaemonUnderTaskman('postgres', \$pgdir.'/bin/postgres -p $port -D '.PROJECTDATA.'/postgres/ -c config_file='.PROJECTENV.'/etc/postgres/postgresql.conf -k '.PROJECTENV.'/var/');

        if(!is_dir(PROJECTDATA.'/postgres/')) {
            passthru(\$pgdir.'/bin/initdb '.PROJECTDATA.'/postgres/');
        }
        passthru(\$pgdir.'/bin/createuser -h \$PROJECTENV/var/ -p $port -S -D -l -R \$PROJECT' );
        passthru(\$pgdir.'/bin/createdb -h \$PROJECTENV/var/ -O \$PROJECT -p $port \$PROJECT');
        passthru('echo "ALTER USER '.__PROJECT__.' WITH PASSWORD \\''.md5(DB::secret.__PROJECT__).'\\';"|'.\$pgdir.'/bin/psql -h \$PROJECTENV/var/ -p $port --user=\$USER \$PROJECT' );
    }

}

EOF;

        file_put_contents(PATH_TMP."/".$RoleClass.".php", $template);
    }

    static function _PostgresStorageMakeSlaveRole() {
        CatchEvent(System_InitConfigs);

        if(isset(self::$external_dsn)) {
            return;
        }

        $RoleClass = __TRAIT__."SlaveHost";
        $__DIR__   = __DIR__;
        $port      = Taskman::getPortFor($RoleClass);

        $template = <<<EOF
<?php

class $RoleClass extends HostRole {

    static function _PostgresBaseDaemon() {
        CatchEvent([$RoleClass::class => System_InitConfigs]);
        \$pgdir = glob('/usr/lib/postgresql/*');
        \$pgdir = end(\$pgdir);

        if(!\$pgdir) {
            throw new Exception("Postgresql not found");
        }

        \$confdir = PROJECTENV.'/etc/postgres_slave/';
        passthru("mkdir -p \$confdir");

        Taskman::prepareConfigs('$__DIR__/etc_slave/', \$confdir, [
            '%PORT%' => $port
        ]);

        Taskman::installDaemonUnderTaskman('postgres', \$pgdir.'/bin/postgres -p $port -D '.PROJECTDATA.'/postgres_slave/ -c config_file='.PROJECTENV.'/etc/postgres_slave/postgresql.conf -k '.PROJECTENV.'/var/');

        if(!is_dir(PROJECTDATA.'/postgres_slave/')) {
            passthru(\$pgdir.'/bin/initdb '.PROJECTDATA.'/postgres_slave/');
        }
        passthru(\$pgdir.'/bin/createuser -h \$PROJECTENV/var/ -p $port -S -D -l -R \$PROJECT' );
        passthru(\$pgdir.'/bin/createdb -h \$PROJECTENV/var/ -O \$PROJECT -p $port \$PROJECT');
        passthru('echo "ALTER USER '.__PROJECT__.' WITH PASSWORD \\''.md5(DB::secret.__PROJECT__).'\\';"|'.\$pgdir.'/bin/psql -h \$PROJECTENV/var/ -p $port --user=\$USER \$PROJECT' );
    }

}

EOF;

        file_put_contents(PATH_TMP."/".$RoleClass.".php", $template);
    }

    static function _PostgresStorageMakePgPoolRole() {
        CatchEvent(System_InitConfigs);

        if(isset(self::$external_dsn)) {
            return;
        }

        $RoleClass = __TRAIT__."PgPoolHost";
        $__DIR__   = __DIR__;
        $port      = Taskman::getPortFor($RoleClass);

        $template = <<<EOF
<?php

class $RoleClass extends HostRole {

    static function _PostgresBaseDaemon() {
        CatchEvent([$RoleClass::class => System_InitConfigs]);
        \$pgdir = glob('/usr/lib/postgresql/*');
        \$pgdir = end(\$pgdir);

        if(!\$pgdir) {
            throw new Exception("Postgresql not found");
        }

        \$confdir = PROJECTENV.'/etc/postgres_pgpool/';
        passthru("mkdir -p \$confdir");

        Taskman::prepareConfigs('$__DIR__/etc_pgpool/', \$confdir, [
            '%PORT%' => $port
        ]);

//        Taskman::installDaemonUnderTaskman('postgres', \$pgdir.'/bin/postgres -p $port -D '.PROJECTDATA.'/postgres_pgpool/ -c config_file='.PROJECTENV.'/etc/postgres_pgpool/postgresql.conf -k '.PROJECTENV.'/var/');

//        if(!is_dir(PROJECTDATA.'/postgres_pgpool/')) {
//            passthru(\$pgdir.'/bin/initdb '.PROJECTDATA.'/postgres_pgpool/');
//        }
//        passthru(\$pgdir.'/bin/createuser -h \$PROJECTENV/var/ -p $port -S -D -l -R \$PROJECT' );
//        passthru(\$pgdir.'/bin/createdb -h \$PROJECTENV/var/ -O \$PROJECT -p $port \$PROJECT');
//        passthru('echo "ALTER USER '.__PROJECT__.' WITH PASSWORD \\''.md5(DB::secret.__PROJECT__).'\\';"|'.\$pgdir.'/bin/psql -h \$PROJECTENV/var/ -p $port --user=\$USER \$PROJECT' );
    }

}

EOF;

        file_put_contents(PATH_TMP."/".$RoleClass.".php", $template);
    }

}
